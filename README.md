# Projet Big Data 
HashMap , BloomFilter & HDFS - Master 2 Logiciels Sûrs - UPEC 

## Auteur

Walid ETTAIEB

## Installation

Executez la commande suivante pour télécharger le projet :

`git clone https://walideco@gitlab.com/walideco/mini_hdfs.git`

## Usage
#### A. Distributed Max Computing
En partant de la racine du projet veuillez executer les ficher shell suivant (dans l'ordre):

1. `~/JARs/namenode/launcher.sh` pour lancer le Namenode.
2. `~/JARs/sever/launcher.sh` pour démarrer les les 5 Datanodes (DN1..5).
3. `~/JARs/client/launcher.sh` pour lancer le client utilisateur puis choisir l'opération à faire ( 0 : Soumission / 1 : Calcul du max )
    * Soumisson : choix d'un ficher via le file picker.
    * Calculer le max : saisir le nom exact du fichier soumis au paravant (example.txt). 

N'oubliez pas de faire une `chmod +x ~/launcher.sh ` pour avoir les permission.

#### B. HashMap et BloomFilter

Vous pouvez tester soit en lançant les tests unitaires soit en executant les JARs suivant :

* `java -jar ~/JARs/hashMap/walid-ettaieb.jar` .
* `java -jar ~/JARs/bloomFilter/walid-ettaieb.jar` . 

## Licence

MIT