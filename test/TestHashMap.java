/**
 * TP n°: 1
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

import hashMap.CharHashMap;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static hashMap.CharHashMap.KEYS_ARRAY;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestHashMap {
  @Test
  @DisplayName("pour la même clef le  marche bien")
  void testPutGetRemove() {
    CharHashMap charHashMap = new CharHashMap();
    charHashMap.put(KEYS_ARRAY[6], 6);

    assertEquals(6, charHashMap.get(KEYS_ARRAY[6]));
    assertEquals(-1, charHashMap.get(KEYS_ARRAY[7]));

    charHashMap.remove(KEYS_ARRAY[6]);

    assertEquals(-1, charHashMap.get(KEYS_ARRAY[6]));

  }
}
