import distMaxComputing.nameNode.NameNode;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.rmi.RemoteException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */
public class TestNameNode {

  @Test
  @DisplayName("Tester la mise à jour du name node")
  void testUpadteReport() {
    NameNode nameNode = new NameNode();
    Map<String, List<String>> map = new HashMap<>();
    List<String> ls = new ArrayList<>();
    ls.add("b1");
    ls.add("b2");
    map.put("f1", ls);
    try {
      nameNode.updateReports("DN1", map);
      assertFalse(nameNode.getDNList().isEmpty());
      assertTrue(nameNode.getDNList().contains("DN1"));
      assertEquals("DN1", nameNode.getDNsOfBlock("b1").get(0));
    } catch (RemoteException e1) {
      e1.printStackTrace();
    }
  }
}
