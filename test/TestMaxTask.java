/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import distMaxComputing.client.MaxTask;
import java.io.*;
import java.nio.charset.StandardCharsets;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class TestMaxTask {
  @Test
  @DisplayName("doit retourner le max d'un fichier")
  void testMaxFromFile() {
    try {

      FileOutputStream fos = null;
      new File(System.getProperty("user.dir")+"/DataNodes/DN1").mkdir();
      new File(System.getProperty("user.dir")+"/DataNodes/DN1/F1").mkdir();
      File f = new File(System.getProperty("user.dir")+"/DataNodes/DN1/F1/BL1");
      fos = new FileOutputStream(f);
      OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
      for (int i = 0; i < 3; i++) {
        osw.write("" + i + "\n");
        osw.flush();
      }
      fos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    MaxTask task = new MaxTask("DN1", "F1", "F1BL1");
    int res = task.execute();
    System.out.println(res);

    assertNotNull(res);
    assertEquals(2, res);
  }
}
