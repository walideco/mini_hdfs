/**
 * TP n°: 2
 * Titre du TP : Bloom Filter
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

import bloomFilter.BloomFilter16;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

public class TestBloomFilter {
  @Test
  @DisplayName("tester si le modulo 16 d'un hexa est correcte")
  void testModulo() {
    String hash1 = "ABC";
    BloomFilter16 bf16 = new BloomFilter16();
    assertEquals(bf16.getModulo16FromHex(hash1), 12);
  }

  @Test
  @DisplayName("tester si le test pour un chaine non existante dans le bF est false")
  void testFalseTrue()  {
    BloomFilter16 bf16 = new BloomFilter16();
    try {
      bf16.insert("A");
      assertNotNull(bf16.testFor("B"));
      assertFalse(bf16.testFor("B"));
      assertTrue(bf16.testFor("A"));
    } catch (NoSuchAlgorithmException e) {
    }
  }
}
