
/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

import distMaxComputing.client.Block;
import distMaxComputing.client.Client;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.swing.plaf.synth.SynthTextAreaUI;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TestClient {
  @Test
  @DisplayName("tester le split")
  void testSplitting() {
    Client client = new Client();
    client.generateExample(4);
    client.getFileManager().setFile(new File(System.getProperty("user.dir") + "/example.txt"));
    try {
      client.getFileManager().split(2);
    } catch (IOException e) {
      e.printStackTrace();
    }
    assertEquals(12, client.getFileManager().getBlocks().get(1).getData().length);
    assertEquals(2, client.getFileManager().getBlocks().size());

    String[] ls = {"b1","b2"};
    List<String> blocks = Arrays.asList(ls);
    String[] ds = {"bn1","dn2","dn3"};
    List<String> dns = Arrays.asList(ds);
    Map<String,List<String>> dist = Client.distribute( client.getFileManager().getBlocks(), dns);
    assertFalse(dist.isEmpty());
    System.out.println(dist);
    assertEquals(dist.get("0").size(), dist.get("1").size());
    assertEquals(3, dist.get("1").size());
  }

}
