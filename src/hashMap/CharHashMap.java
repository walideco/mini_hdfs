package hashMap;

import java.util.Random;

import static java.lang.System.out;

/**
 * TP n°: 1
 * Titre du TP : Hash Map
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

public class CharHashMap {

  static public final char EMPTY_CHAR = '\u0000';
  static private final int HASH_MODULO = 11;
  static public final char[] KEYS_ARRAY =
      {
          'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
          'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
          'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
          'Y', 'Z'
      };
  private int size;
  private char[] keys;

  private int[] values;

  public CharHashMap() {
    this.size = KEYS_ARRAY.length + 1;
    this.keys = new char[size];
    this.values = new int[size];
    for (int i = 0; i < size; i++) {
      keys[i] = EMPTY_CHAR;
      values[i] = -1;
    }
  }

  private int hash(char key) {
    return key % HASH_MODULO;
  }

  public void put(char key, int value) {
    int index = hash(key);
    while (keys[index] != EMPTY_CHAR)
      index = (index + 1) % size;
    keys[index] = key;
    values[index] = value;
  }

  public int get(char key) {
    int index = hash(key);
    int value = -1; // value to return if the key is not in the map
    while (keys[index] != key && keys[index] != EMPTY_CHAR)
      index = (index + 1) % size;
    if (keys[index] == key) {
      value = values[index];
    }
    return value;
  }

  public void remove(char key) {
    int index = hash(key);
    while (keys[index] != key && keys[index] != EMPTY_CHAR) {
      index = (index + 1) % size;
    }
    keys[index] = EMPTY_CHAR;
    values[index] = -1;
    index = (index + 1) % size;
    while (keys[index] != EMPTY_CHAR) {
      char savedKey = keys[index];
      int savedValue = values[index];
      keys[index] = EMPTY_CHAR;
      values[index] = -1;
      put(savedKey, savedValue);
      index = (index + 1) % size;
    }
  }

  @Override
  public String toString() {
    String str = "---------\nKey| Val\n";
    for (int i = 0; i < size; i++) {
      str += (keys[i] != EMPTY_CHAR) ? " " + keys[i] + " | " + values[i] + " " : " _ | _";
      str += "\n";
    }
    str += "---------";
    return str;
  }

  public static void main(String[] args) {
    CharHashMap charHashMap = new CharHashMap();
    Random r = new Random();
    for (int i=0; i<10;i++) {
      int index = r.nextInt(25);
      out.println("'put("+KEYS_ARRAY[index]+")");
      charHashMap.put(KEYS_ARRAY[index],index);
      out.println(charHashMap);
    }
    out.println(charHashMap);

    out.print("'get( 'F' ) = "+charHashMap.get('F'));

    charHashMap.remove('O');
    out.println(charHashMap);
  }

}
