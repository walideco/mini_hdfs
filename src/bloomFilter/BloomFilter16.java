package bloomFilter;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.CRC32;

import static java.lang.System.out;

/**
 * TP n°: 2
 * Titre du TP : Bloom Filter
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

// TODO Tests unitaires

public class BloomFilter16 {

  private boolean[] filter;

  public BloomFilter16() {
    this.filter = new boolean[16];
  }


  String bytesToHex(byte[] bytes) {
    final StringBuilder builder = new StringBuilder();
    for (byte b : bytes) {
      builder.append(String.format("%02x", b));
    }
    return builder.toString();
  }

  String getHexMassageDigest(String algo, byte[] input) throws NoSuchAlgorithmException {
    MessageDigest md = MessageDigest.getInstance(algo);
    return bytesToHex(md.digest(input));
  }

  String getHexCRC32(byte[] input) {
    CRC32 crc32 = new CRC32();
    crc32.update(input);
    return Long.toHexString(crc32.getValue());
  }

  public int getModulo16FromHex(String hash) {
    return Integer.valueOf(hash.substring(hash.length() - 1), 16);
  }

  public void insert(String value) throws NoSuchAlgorithmException {
    String hash1 = getHexMassageDigest("MD5", value.getBytes());
    int index1 = getModulo16FromHex(hash1);
    filter[index1] = true;
    String hash2 = getHexMassageDigest("SHA-1", value.getBytes());
    int index2 = getModulo16FromHex(hash2);
    filter[index2] = true;
    String hash3 = getHexCRC32(value.getBytes());
    int index3 = getModulo16FromHex(hash3);
    filter[index3] = true;
  }

  public boolean testFor(String value) throws NoSuchAlgorithmException {
    String hash1 = getHexMassageDigest("MD5", value.getBytes());
    int index1 = getModulo16FromHex(hash1);
    if (!filter[index1])
      return false;
    String hash2 = getHexMassageDigest("SHA-1", value.getBytes());
    int index2 = getModulo16FromHex(hash2);
    if (!filter[index2])
      return false;
    String hash3 = getHexCRC32(value.getBytes());
    int index3 = getModulo16FromHex(hash3);
    return filter[index3];
  }

  @Override
  public String toString() {
    StringBuilder str = new StringBuilder("Bloom Filter = [");
    for (boolean bool : filter) {
      str.append((bool) ? " 1" : " 0");
    }
    str.append(" ]");
    return str.toString();
  }

  public static void main(String[] args) {
    BloomFilter16 bloomFilter = new BloomFilter16();
    try {
      out.println("'Insert of \"a\" ");
      bloomFilter.insert("a");
      out.println(bloomFilter);

      out.println("'Insert of \"b\" ");
      bloomFilter.insert("b");
      out.println(bloomFilter);

      out.println("'Insert of \"y\" ");
      bloomFilter.insert("y");
      out.println(bloomFilter);

      out.println("'Insert of \"l\" ");
      bloomFilter.insert("l");
      out.println(bloomFilter);

      out.print("'Testing for \"q\" = ");
      out.println(bloomFilter.testFor("q"));

      out.print("'Testing for \"z\" = ");
      out.println(bloomFilter.testFor("z"));

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
  }


}
