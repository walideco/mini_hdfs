package distMaxComputing.nameNode;

/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

import distMaxComputing.share.ClientProtocolInterface;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

import static java.lang.System.err;
import static java.lang.System.out;

public class NameNode implements ClientProtocolInterface {
  private static final int REGISTRY_PORT = 12345;
  private List<String> DNList;
  private HashMap<String, List<String>> namespace;
  private HashMap<String, List<String>> inodes;

  public NameNode() {
    this.DNList = new ArrayList<>();
    this.namespace = new HashMap<>();
    this.inodes = new HashMap<>();
  }


  private void updateMap(Map<String, List<String>> map, String key, String value) {
    if (map.containsKey(key)) {
      map.computeIfPresent(key, (k, v) -> {
        if (!v.contains(value)) {
          v.add(value);
        }
        return v;
      });
    } else {
      List<String> newListBL = new ArrayList<>();
      newListBL.add(value);
      map.put(key, newListBL);
    }
  }


  @Override
  public void updateReports(String dataNodeName, Map<String, List<String>> report) throws RemoteException {
    if(report==null)
      return;
    if (!DNList.contains(dataNodeName))
      DNList.add(dataNodeName);
    Set<String> dirs = report.keySet();
    for (String dir : dirs) {
      List<String> blocks = report.get(dir);
      for (String block : blocks) {
        String blockId = dir.concat(block);
        updateMap(namespace, dir, blockId);
        updateMap(inodes, blockId, dataNodeName);
      }
    }
  }

  @Override
  public List<String> getDNList() throws RemoteException {
    return this.DNList;
  }

  @Override
  public void registerDN(String nameDN) throws RemoteException {
    if (DNList.contains(nameDN))
      throw new RemoteException("NameNode " + nameDN + "is already exist");
    DNList.add(nameDN);
  }

  @Override
  public List<String> getBlocksOfFile(String fileName) throws RemoteException {
    List<String> blocks = namespace.getOrDefault(fileName.substring(0, fileName.lastIndexOf(".")), null);
    if (blocks == null || blocks.isEmpty()) {
      throw new RemoteException("The blocks of '" + fileName + "' is not exist");
    }
    return blocks;
  }

  @Override
  public List<String> getDNsOfBlock(String blockName) throws RemoteException {
    List<String> dataNodes = inodes.getOrDefault(blockName, null);
    if (dataNodes == null || dataNodes.isEmpty()) {
      throw new RemoteException("The DataNodes list of '" + blockName + "' is not exist");
    }
    return dataNodes;
  }

  public static void main(String[] args) {
    NameNode nameNode = new NameNode();
    try {
      Registry reportsRegistry = LocateRegistry.createRegistry(REGISTRY_PORT);
      ClientProtocolInterface nn = (ClientProtocolInterface) UnicastRemoteObject
          .exportObject(nameNode, REGISTRY_PORT);
      reportsRegistry.rebind("NameNode", nn);
      out.println("NameNode is running at port n° "+ REGISTRY_PORT);
    } catch (RemoteException e) {
      err.println("Impossible de démarrer le NameNode : " + e.getMessage());
    }
  }
}
