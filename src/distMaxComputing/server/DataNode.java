package distMaxComputing.server;

import distMaxComputing.share.ClientProtocolInterface;
import distMaxComputing.share.DataNodeProtocolInterface;
import distMaxComputing.share.Task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.stream.Stream;

import static java.lang.System.*;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

public class DataNode implements DataNodeProtocolInterface {
  private static int REGISTRY_PORT = 12345;
  private static int INITIAL_PORT = 10000;
  private static int HEARTBEAT_SLOT = 5000;
  private String name;
  private File directory;
  private int remotePort;

  public DataNode(int id) {
    this.name = "DN" + id;
    this.remotePort = INITIAL_PORT + id;
    File dir = new File(System.getProperty("user.dir") + "/DataNodes");
    if (!dir.exists() && !dir.mkdir())
      throw new SecurityException("Impossible to create the DataNodes Directory");
    dir = new File(System.getProperty("user.dir") + "/DataNodes/" + name);
    if (!dir.exists() && !dir.mkdir())
      throw new SecurityException("Impossible to create the DataNode Directory");
    this.directory = dir;
  }


  private List<String> getBlocksFromDir(File dir) {
    File[] files = dir.listFiles(File::isFile);
    return (files == null)
        ? null
        : Stream.of(files).map(File::getName).collect(toList());
  }

  private Map<String, List<String>> reporting() {
    File[] blocks = directory.listFiles(File::isDirectory);
    return (blocks == null)
        ? null
        : Stream.of(blocks)
        .filter(f -> !f.getName().startsWith("."))
        .collect(toMap(File::getName, this::getBlocksFromDir));
  }

  private void sendReport() throws RemoteException, NotBoundException {
    Registry reporterRegistry = LocateRegistry.getRegistry(REGISTRY_PORT);
    ClientProtocolInterface nameNode = (ClientProtocolInterface) reporterRegistry.lookup("NameNode");
    Map<String, List<String>> report = reporting();
    nameNode.updateReports(name, report);
  }

  private void heartBeat() throws RemoteException, NotBoundException {
    Registry reporterRegistry = LocateRegistry.getRegistry(REGISTRY_PORT);
    ClientProtocolInterface nameNode = (ClientProtocolInterface) reporterRegistry.lookup("NameNode");
    nameNode.registerDN(this.name);
  }

  private void activateReporting() {
    Timer timer = new Timer();
    TimerTask timerTask = new TimerTask() {
      @Override
      public void run() {
        try {
          sendReport();
        } catch (RemoteException | NotBoundException ignored) {
        }
      }
    };
    timer.schedule(timerTask, 0, HEARTBEAT_SLOT);
  }

  private File prepareBlockFile(String fileName, String blockName) {
    if (!this.directory.isDirectory() || !this.directory.canWrite())
      throw new SecurityException("Impossible saving operation");
    File dir = new File(directory.getAbsolutePath() + "/"
        + fileName.substring(0, fileName.lastIndexOf(".")));
    if (!dir.exists() && !dir.mkdir())
      throw new SecurityException("Impossible to create the " + fileName + " directory");
    return new File(directory.getAbsolutePath()
        + "/" + fileName.substring(0, fileName.lastIndexOf("."))
        + "/" + blockName);
  }

  @Override
  public void writeBlock(String fileName, String blockName, byte[] data, List<String> pipelineDNs) throws RemoteException {
    File blockFile = prepareBlockFile(fileName, blockName);
    try (FileOutputStream fos = new FileOutputStream(blockFile)) {
      fos.write(data);
      try {
        this.sendReport();
      } catch (NotBoundException ignored) {
      }
      if (pipelineDNs != null && !pipelineDNs.isEmpty()) {
        String nextDN = pipelineDNs.get(0);
        List<String> nexts = pipelineDNs.stream().filter(s -> !nextDN.equals(s)).collect(toList());
        Registry registry = LocateRegistry.getRegistry(REGISTRY_PORT);
        DataNodeProtocolInterface dn = (DataNodeProtocolInterface) registry.lookup(nextDN);
        dn.writeBlock(fileName, blockName, data, nexts);
      }
    } catch (IOException e) {
      throw new RemoteException("Writing Opretion on DataNode " + this.name + " is failed :" + e.getMessage());
    } catch (NotBoundException e) {
      throw new RemoteException("Replication error:" + e.getMessage());
    }
  }

  @Override
  public <T> T compute(Task<T> task) throws RemoteException {
    return task.execute();
  }

  public static void main(String[] args) throws RemoteException, NotBoundException {
    if(args.length == 0) {
      err.println("error: First parameter of DataNode program is missing ! ");
      exit(1);
    }
    Registry registry;
    try {
      registry = LocateRegistry.createRegistry(REGISTRY_PORT);
    } catch (RemoteException e) {
      registry = LocateRegistry.getRegistry(REGISTRY_PORT);
    }
    int dataNodeID = 0;
    try {
      dataNodeID =  Integer.parseInt(args[0]); // setDataNodeId();
    } catch (NumberFormatException e) {
      err.println("The ID of DataNode must be an integer : '" + args[0] + "' found ");
      exit(1);
    }
    try {
      registry.lookup("DN" + dataNodeID);
      err.println("The ID of DataNode '" + dataNodeID + "' is already exist ");
      exit(1);
    } catch (NotBoundException ignored) {
    }
    DataNode dataNode = new DataNode(dataNodeID);

    DataNodeProtocolInterface dn = (DataNodeProtocolInterface) UnicastRemoteObject
        .exportObject(dataNode, dataNode.remotePort);
    registry.rebind(dataNode.name, dn);
    out.println("DataNode " + dataNode.name + " is running at port n° " + dataNode.remotePort + "...");
    dataNode.heartBeat();
    dataNode.activateReporting();
  }
}