package distMaxComputing.share;

/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;


public interface ClientProtocolInterface extends Remote {
  void updateReports(String dataNodeName, Map<String, List<String>> blocks) throws RemoteException;

  void registerDN(String nameDN) throws RemoteException;

  List<String> getDNList() throws RemoteException;

  List<String> getBlocksOfFile(String fileName) throws RemoteException;

  List<String> getDNsOfBlock(String blockName) throws RemoteException;
}
