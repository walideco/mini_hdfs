package distMaxComputing.share;

/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface DataNodeProtocolInterface extends Remote {
  void writeBlock(String fileName, String blockName, byte[] data, List<String> pipelineDNs) throws RemoteException;

  <T> T compute(Task<T> task) throws RemoteException;
}
