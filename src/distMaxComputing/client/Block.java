package distMaxComputing.client;

/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

public class Block {

  private String fileName;
  private String blockId;
  private long offset;
  private byte[] data;

  Block(String fileName, String blockId, long offset, byte[] data) {
    this.fileName = fileName;
    this.blockId = blockId;
    this.offset = offset;
    this.data = data;
  }

  String getBlockId() {
    return blockId;
  }

  public String getFileName() {
    return fileName;
  }

  public byte[] getData() {
    return data;
  }

  @Override
  public String toString() {
    return "[File: " + fileName + " Block: " + blockId + ",Offset: " + offset + ",Size: " + data.length + " Bytes]";
  }
}
