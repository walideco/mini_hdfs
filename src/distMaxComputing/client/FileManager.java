package distMaxComputing.client;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

public class FileManager {

  private File file;
  private List<Block> blocks;

  FileManager() {
    file = null;
    blocks = new ArrayList<>();
  }

  public void setFile(File file) {
    if (file == null)
      throw new IllegalArgumentException("No path found");
    if (!file.exists() || !file.canRead() || !file.isFile())
      throw new SecurityException(file.getAbsolutePath());
    this.file = file;
  }

  public List<Block> getBlocks() {
    return blocks;
  }

  public void split(int sizeOfBlock) throws IOException {
    String fileName = file.getName();
    BufferedReader br = Files.newBufferedReader(Paths.get(fileName));

    int offset = 0, blockId = 0;
    int nbRead = 0;
    String line;
    String block = "";
    while ((line = br.readLine()) != null) {
      block += line + "\n";
      nbRead++;
      if (nbRead % sizeOfBlock == 0) {
        blocks.add(
            new Block(fileName, "" + blockId, offset, block.getBytes(StandardCharsets.UTF_8))
        );
        blockId++;
        block = "";
        offset = blockId * sizeOfBlock;
      }
    }
    br.close();
  }

  @Override
  public String toString() {
    return "[ " +
        "FileManager of :" + file.getName() +
        ", Blocks: " + blocks +
        " ]";
  }
}
