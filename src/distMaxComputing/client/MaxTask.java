package distMaxComputing.client;

import distMaxComputing.share.Task;

/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class MaxTask implements Task<Integer>, Serializable {
  private static final long serialVersionUID = 39429L;
  private String dnName;
  private String fileName;
  private String blockName;

  public MaxTask(String dnName, String fileName, String blockName) {
    if (fileName.indexOf('.') != -1)
      throw new IllegalArgumentException("The name file for MaxTask Can not contain a '.'");
    this.dnName = dnName;
    this.fileName = fileName;
    this.blockName = blockName;
  }



  @Override
  public Integer execute() {
    String blockId = blockName.substring(fileName.length());
    String path = System.getProperty("user.dir")
        + "/DataNodes"
        + "/" + dnName
        + "/" + fileName
        + "/" + blockId;
    try (IntStream stream = Files.lines(Paths.get(path)).mapToInt(Integer::valueOf)) {
      OptionalInt max = stream.max();

      if (max.isPresent())
        return max.getAsInt();
      return null;
    } catch (IOException e) {
      return null;
    }
  }
}
