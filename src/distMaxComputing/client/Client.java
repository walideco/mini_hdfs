package distMaxComputing.client;

/**
 * TP n°: 3
 * Titre du TP : Distributed Max Computing
 * Date : 18/03/17
 * Nom : ETTAIEB
 * Prenom : Walid
 * email : ettaieb.walid@gmail.com
 * Remarques : **
 */


import distMaxComputing.share.ClientProtocolInterface;
import distMaxComputing.share.DataNodeProtocolInterface;
import distMaxComputing.share.Task;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;

import static java.lang.System.err;
import static java.lang.System.exit;
import static java.lang.System.out;
import static java.util.stream.Collectors.toList;


public class Client {
  private static final int REPLICATION_FACTOR = 3;
  private static final int BLOCK_SIZE = 64; // Taille en nombre d'entier
  private static final int REGISTRY_PORT = 12345; // Port du registry

  private FileManager fileManager = new FileManager();

  public Client() {
    this.fileManager = new FileManager();
  }

  public FileManager getFileManager() {
    return fileManager;
  }

  static public HashMap<String, List<String>> distribute(List<Block> blocks, List<String> dataNodes) {
    HashMap<String, List<String>> dist = new HashMap<>();
    int nbBlock = blocks.size();
    for (int i = 0; i < nbBlock; i++) {
      ArrayList<String> ls = new ArrayList<>(REPLICATION_FACTOR);
      for (int j = 0; j < REPLICATION_FACTOR; j++)
        ls.add(dataNodes.get((i + j) % dataNodes.size()));
      dist.put(blocks.get(i).getBlockId(), ls);
    }
    return dist;
  }

  public void generateExample(int nbOfInteger) {
    File file = new File(System.getProperty("user.dir") + "/example.txt");
    if (file.isFile() && file.exists())
      return;
    FileOutputStream fos = null;
    try {
      Random r = new Random();
      fos = new FileOutputStream(file);
      OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
      for (int i = 0; i < nbOfInteger; i++) {
        String s = "" + r.nextInt(50000);
        osw.write(s + "\n");
        osw.flush();
      }
      fos.close();
      out.println("Fichier "+file.getAbsolutePath()+" de longueur "+ nbOfInteger+" entiers est généré !");
    } catch (IOException e) {
      err.println("Impossible to generate the example.txt file ! Please check your permission ");
    }
  }

  void uploadFile(File file) throws NotBoundException, IOException {

    fileManager.setFile(file);
    fileManager.split(BLOCK_SIZE);
    out.println(fileManager.getBlocks().size() + " bocks");
    Registry registry = LocateRegistry.getRegistry(REGISTRY_PORT);
    ClientProtocolInterface nameNode = (ClientProtocolInterface) registry.lookup("NameNode");

    List<String> listDN = nameNode.getDNList();
    HashMap<String, List<String>> distMap = distribute(fileManager.getBlocks(), listDN);

    out.println("Envoie ...");
    for (Block block : fileManager.getBlocks()) {
      List<String> blockDNs = distMap.getOrDefault(block.getBlockId(), null);
      if (blockDNs == null || blockDNs.isEmpty()) {
        throw new NotBoundException("The list of DataNode for this Block is empty");
      }
      String dnId = blockDNs.get(0);
      DataNodeProtocolInterface dn = (DataNodeProtocolInterface) registry.lookup(dnId);
      List<String> nextDNs = blockDNs.stream().filter(s -> blockDNs.indexOf(s) != 0).collect(toList());
      try {
        dn.writeBlock(block.getFileName(), block.getBlockId(), block.getData(), nextDNs);
      } catch (RemoteException e) {
        throw new IOException("Soumisson du bloc" + block + " echooué :\n " + e.getMessage());
      }
    }
  }


  int computeMaxOfFile(String fileName) throws RemoteException, NotBoundException {
    Registry registry = LocateRegistry.getRegistry(REGISTRY_PORT);
    ClientProtocolInterface nameNode = (ClientProtocolInterface) registry.lookup("NameNode");
    List<String> blocks = nameNode.getBlocksOfFile(fileName);
    List<Integer> results = new ArrayList<>();
    for (String block : blocks) {
      String dataNodeName = nameNode.getDNsOfBlock(block).get(0);
      DataNodeProtocolInterface dataNode = (DataNodeProtocolInterface) registry.lookup(dataNodeName);
      Task<Integer> task = new MaxTask(dataNodeName, fileName.substring(0, fileName.lastIndexOf(".")), block);
      Integer subMax = dataNode.compute(task);
      if (subMax == null)
        throw new RemoteException("The computing of max of '" + block + "' block is failed");
      results.add(subMax);
    }
    OptionalInt OptionalMaxValue = results.stream().mapToInt(Integer::intValue).max();
    if (OptionalMaxValue.isPresent())
      return OptionalMaxValue.getAsInt();
    else
      throw new RemoteException("The computing of max of '" + fileName + "' file is failed");
  }

  static File pickFile() {
    JFileChooser jFileChooser = new JFileChooser(FileSystemView.getFileSystemView());
    jFileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
    jFileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
    jFileChooser.showOpenDialog(jFileChooser);
    return jFileChooser.getSelectedFile();
  }

  static private String setFileToDownload() {
    Scanner sc = new Scanner(System.in);
    System.out.println("Veuillez saisir un nom de fichier (example.txt) :");
    return sc.nextLine();
  }

  static private String choiceOperation() {
    Scanner sc = new Scanner(System.in);
    System.out.println("Veuillez saisir '0' pour envoyer un fichier ou '1' pour calculer le max :");
    return sc.nextLine();
  }


  public static void main(String[] args) {
    Client client = new Client();
    client.generateExample(4096);
    while (true) {
      String choice = choiceOperation();
      if (!(choice.equals("0") || choice.equals("1"))) {
        err.println("code d'opération non valide ( 0 / 1 ) ! ");
        exit(1);
      }
      if ("0".equals(choice))
        try {
          File file = pickFile();
          if(file==null || !file.getName().endsWith(".txt")) {
            out.println("fichier non valide!");
            continue;
          }
          out.println("Soumission de fichier'" + file.getName() + "'");
          out.println("Decoupage ...");
          client.uploadFile(file);
          out.println("Fait !");
        } catch (IOException | NotBoundException e) {
          err.println(e.getMessage());
        }
      else
        try {
          String fileToCompute = setFileToDownload();
          if(!fileToCompute.endsWith(".txt")) {
            out.println("Que les fichiers txt ne peuvent être traités!");
            continue;
          }
          out.println("Calcul du max du fichier '" + fileToCompute + "'");
          int max = client.computeMaxOfFile(fileToCompute);
          out.println("Le max de '" + fileToCompute + "' est : " + max);
        } catch (NotBoundException | IOException e) {
          err.println(e.getMessage());
        }
    }
  }
}
